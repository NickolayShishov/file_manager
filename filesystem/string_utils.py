import random


def get_random_string(length=30):
    sys_random = random.SystemRandom()
    valid_symbols = '1 2 3 4 5 6 7 8 9  0 q w e r t y u i o p d a s d c v x n m'
    valid_symbols = valid_symbols.split(' ')
    symbol_array = []
    for _ in range(length):
        symbol_array.append(sys_random.choice(valid_symbols))
    return ''.join(symbol_array)
