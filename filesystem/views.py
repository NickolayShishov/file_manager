from django.core.files import File
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render, reverse
from django.contrib.auth.decorators import login_required


from .forms import UploadFileForm
from .models import UserFileModel


@login_required()
def add_file(request):
    context = {}
    if request.method == "POST":
        user = request.user
        f = File(request.FILES['src'])
        new_user_file = UserFileModel()
        new_user_file.owner = user
        if not f:
            form = UploadFileForm()
            context['file_upload_form'] = form
            return render(request, template_name='edit.html', context=context)
        new_user_file.save(f)
        return HttpResponseRedirect(reverse('index'))
    if request.method == "GET":
        context['file_upload_form'] = UploadFileForm()
        return render(
                request=request,
                template_name='edit.html',
                context=context)


@login_required()
def download_file(request, id):
    user = request.user
    f = UserFileModel.objects.filter(id=id).first()
    if not f:
        return HttpResponseForbidden()
    if f.owner != user:
        return HttpResponseForbidden()
    file_name = f.virtual_name
    response = HttpResponse(f.src, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % file_name
    return response


@login_required()
def delete_file(request, id):
    user = request.user
    f = UserFileModel.objects.filter(id=id, owner=user).first()
    if not f:
        return HttpResponseForbidden()
    f.delete()
    return HttpResponseRedirect(reverse('index'))


@login_required()
def make_file_public_available(request, id):
    user = request.user
    f = UserFileModel.objects.filter(id=id, owner=user).first()
    if f:
        f.create_or_update_public_link()
    return HttpResponseRedirect(reverse('index'))


@login_required()
def make_file_public_unavailable(request, id):
    user = request.user
    f = UserFileModel.objects.filter(id=id, owner=user).first()
    if f:
        f.delete_public_link()
    return HttpResponseRedirect(reverse('index'))


def public_file_access(request, public_link):
    f = UserFileModel.objects.filter(public_link=public_link).first()
    if not f:
        return HttpResponseNotFound()
    file_name = f.virtual_name
    response = HttpResponse(f.src, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % file_name
    return response
