from django import forms


class UploadFileForm(forms.Form):
    src = forms.FileField()
