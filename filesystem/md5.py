import hashlib


def CalculateFileMd5(f, chunk_size=4096):
    """
        Calcluate md5 sum of file
    """
    md5_sum = hashlib.md5()
    for chunk in iter(lambda: f.read(chunk_size), b""):
        md5_sum.update(chunk)
    return md5_sum.hexdigest()
