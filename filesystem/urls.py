from django.urls import include, path

from . import views

urlpatterns = [
    path('create/', views.add_file, name='create'),
    path('download/<str:id>', views.download_file, name='download'),
    path('delete/<str:id>', views.delete_file, name='delete'),
    path('create_link/<str:id>', views.make_file_public_available, name='create_link'),
    path('remove_link/<str:id>', views.make_file_public_unavailable, name='remove_link'),
    path('public_access/<str:public_link>', views.public_file_access, name='public_access')
]
