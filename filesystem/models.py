import uuid

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.dispatch import receiver

from .string_utils import get_random_string
from .md5 import CalculateFileMd5


class UserFileModel(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        db_column='owner')
    virtual_name = models.CharField(max_length=200)
    md5_sum = models.CharField(max_length=32)
    public_link = models.CharField(max_length=32, default='')
    src = models.FileField()

    class Meta:
        indexes = [
            models.Index(fields=['id']),
            models.Index(fields=['md5_sum'])
        ]

    def create_or_update_public_link(self):
        new_public_link = get_random_string()
        self.public_link = new_public_link
        self.save()
        return new_public_link

    def delete_public_link(self):
        self.public_link = ''
        self.save()

    def save(self, file=None):
        if not file:
            if not self.src:
                self.delete()
                return
            file = self.src.file
        if not self.virtual_name:
            file_name = file.name.split('/')[-1]
            if len(file_name) > 200:
                parts = file_name.split('.')
                parts[0]=parts[0][:199-len(parts[1])]
                file_name = '.'.join(parts)
            self.virtual_name = file_name
        self.md5_sum = CalculateFileMd5(file.file)
        same = UserFileModel.objects.filter(md5_sum=self.md5_sum)
        if not same:
            self.src.save(
                name=self.virtual_name,
                content=file,
                save=False)
        else:
            self.src = same[0].src
        super(UserFileModel, self).save()

    def __str__(self):
        return "owner: %s name: %s path: %s md5: %s" % (
            self.owner,
            self.virtual_name,
            str(self.src),
            self.md5_sum)


@receiver(models.signals.post_delete, sender=UserFileModel)
def delete(sender, instance, **kwargs):
    same = UserFileModel.objects.filter(md5_sum=instance.md5_sum)
    if not same:
        instance.src.delete()
