from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from filesystem.models import UserFileModel


def index(request):
    context = dict()
    if request.method == 'GET':
        user = request.user
        if user.is_authenticated:
            files = UserFileModel.objects.filter(owner=user)
            context['files'] = sorted(files, key=lambda x: x.id)
        else:
            return HttpResponseRedirect(reverse('authsystem:login'))
        return render(request, template_name='index.html', context=context)
