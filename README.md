## How to run project on your computer

First, clone repository to your project 


1. **Install virtual enviroment**

    On *Ubuntu*
    Install program called *virtualenv* with apt-get. Use *mkvirtualenv* command to create virtual enviroment. Use *workon* command to work on virtual enviroment.

2. **Install requiroments** 

    Install *python 3* in your virtual enviroment. Install *pip3* to manage your dependendencies. Open your console in your project's folder and type *pip3 sync requirements.txt*
    Note that *sync* command will delete all packages that have been installed and are not presented in *requirements.txt*

3. **Run server** 

    While in configured python virtual enviroment, type *python3 manage.py runserver* and test server will be started, you can test project now
