from django.shortcuts import render
import django.contrib.auth as auth
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, reverse
# Create your views here.


def login(request):
    current_user = request.user
    if request.method == 'POST':
        login_form = auth.forms.AuthenticationForm(data=request.POST)
    else:
        login_form = auth.forms.AuthenticationForm()
    context = dict()
    context['login_form'] = login_form
    if request.method == 'POST':
        # logout if user was authenticated
        if current_user.is_authenticated:
            auth.logout(request)
        # if form is not valid, return
        if not login_form.is_valid():
            return render(
                request,
                template_name='login.html',
                context=context
            )
        # trying to authorize
        user = auth.authenticate(
            username=request.POST['username'],
            password=request.POST['password']
        )
        if user is None:
            login_form.add_error(None, "Incorrect login or password")
            return render(request,
                          template_name='login.html',
                          context=context)
        else:
            # attach user to session
            auth.login(request, user)
            return HttpResponseRedirect(reverse('index'))

    if current_user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))
    else:
        return render(request,
                      template_name='login.html',
                      context=context)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('index'))


def register(request):
    # We create form only with POST statement, because
    # form created with {} is recognized as bound one
    if request.method == 'POST':
        register_form = auth.forms.UserCreationForm(data=request.POST)
    else:
        register_form = auth.forms.UserCreationForm()

    context = dict()
    context['register_form'] = register_form
    if request.method == 'POST':
        # if no validation_errors
        if not register_form.is_valid():
            return render(request, template_name='register.html',
                          context=context)
        # Creating new user
        user = auth.models.User.objects.create_user(
            username=request.POST['username'],
            email=None,
            password=request.POST['password1'])
        # Saving new user in database
        user.save()
        # attaching user to session
        auth.login(request, user)
        return HttpResponseRedirect(reverse('index'))

    return render(request, template_name='register.html',
                  context=context)
