from django.test import TestCase
import django.contrib.auth as auth
from django.urls import reverse
from .apps import AuthSystemConfig

APPLICATION_NAME = AuthSystemConfig.name


class LoginTestCase(TestCase):

    def setUp(self):
        self.test_user = auth.models.User.objects.create_user(
            username='Testuser',
            password='testpassword'
        )

    def tearDown(self):
        self.client.logout()

    def testCorrectUser(self):
        data = {
            'username': 'Testuser',
            'password': 'testpassword'
        }
        response = self.client.post(reverse('%s:login' % APPLICATION_NAME), data)
        self.assertEqual(
            self.test_user,
            auth.get_user(self.client)
        )
        self.assertRedirects(response, reverse('index'))

    def testIncorrectUserName(self):
        data = {
            'username': 'incorrect name',
            'password': 'testpassword'
        }
        response = self.client.post(reverse('%s:login' % APPLICATION_NAME), data)
        self.assertFalse(self.test_user == auth.get_user(self.client))
        self.assertTrue(len(response.context['login_form'].errors) > 0)

    def testIncorrectPassword(self):
        data = {
            'username': 'Testuser',
            'password': 'wrongpassword'
        }
        response = self.client.post(
            reverse('%s:login' % APPLICATION_NAME),
            data
        )
        self.assertFalse(self.test_user == auth.get_user(self.client))
        self.assertTrue(len(response.context['login_form'].errors) > 0)

    def testLogout(self):
        self.client.login(
            credentials={
                'username': 'Testuser',
                'password': 'wrongpassword'
            }
        )
        response = self.client.post(reverse('%s:logout' % APPLICATION_NAME))
        self.assertFalse(self.test_user == auth.get_user(self.client))


class RegisterTestCase(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCorrectRegister(self):
        data = {
            'username': 'TestUser',
            'password1': 'testpassword',
            'password2': 'testpassword'
        }
        response = self.client.post(reverse('%s:register' % APPLICATION_NAME), data)
        user = auth.models.User.objects.filter(username=data['username'])
        self.assertFalse(not user)

    def testUserAlreadyExists(self):
        some_user = auth.models.User.objects.create_user(
            username='creator',
            password='creation'
        )
        data = {
            'username': 'creator',
            'password1': 'testpassword',
            'password2': 'testpassword'
        }
        response = self.client.post(reverse('%s:register' % APPLICATION_NAME), data)
        some_user.delete()
        user = auth.models.User.objects.filter(username=data['username'])
        self.assertTrue(not user)

    def testUserDifferentPasswords(self):
        data = {
            'username': 'TestUser',
            'password1': 'testpassword',
            'password2': 'wrongpassword'
        }
        response = self.client.post(reverse('%s:register' % APPLICATION_NAME), data)
        user = auth.models.User.objects.filter(username=data['username'])
        self.assertTrue(not user)
