from django.apps import AppConfig


class AuthSystemConfig(AppConfig):
    name = 'authsystem'
